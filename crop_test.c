#include <time.h>
#include "farbfeld.h"
#include "rgbafun.h"
#include "copyfun.h"

int
main(int argc, char *argv[]) {
    /* arguments */
    if (argc != 3) {
        fprintf(stderr, "usage: %s input_file output_file\n", argv[0]);
        return 1;
    }

    uint32_t width = 0, height = 0;
    uint32_t hdr[4];
    uint16_t *image = farbfeld_read(argv[1], hdr, &width, &height);
    if (image == NULL) {
        fprintf(stderr, "error reading image: %s\n", argv[1]);
    }

    uint32_t region_width, region_height;
    uint16_t *image_region = farbfeld_get_region(image, width, height, 0.2f, 0.2f, 0.8f, 0.8f, &region_width,
                                                 &region_height);
    image_region = farbfeld_copy(copy_flip_horizontal_and_vertical, image_region, region_width, region_height, &width);
    uint32_t *region_hdr = farbfeld_make_header(region_width, region_height);
    farbfeld_write(argv[2], region_hdr, image_region, region_width, region_height);
    if (errno == 1) {
        fprintf(stderr, "error writing image: %s\n", argv[2]);
    }

    free(region_hdr);
    free(image_region);
    free(image);

    return 0;
}
