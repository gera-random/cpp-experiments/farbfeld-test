#include <time.h>
#include "farbfeld.h"

uint16_t *get_rgba(uint32_t x, uint32_t y, uint32_t width, uint32_t height) {
    uint16_t *rgba = malloc(4 * sizeof(uint16_t));
    rgba[0] = x * UINT16_MAX / width;
    rgba[1] = y * UINT16_MAX / height;
    rgba[2] = (x + y) * UINT16_MAX / (width + height);
    rgba[3] = UINT16_MAX;
    return rgba;
}

int
main(int argc, char *argv[]) {
    srand(clock());

    /* arguments */
    if (argc != 2) {
        fprintf(stderr, "usage: %s output_file\n", argv[0]);
        return 1;
    }

    uint32_t width = 100, height = 100;

    uint32_t *hdr = farbfeld_make_header(width, height);
    uint16_t *image = farbfeld_generate_image(get_rgba, width, height);

    farbfeld_write(argv[1], hdr, image, width, height);
    if (errno == 1) {
        fprintf(stderr, "error writing image: %s\n", argv[2]);
    }

    free(hdr);
    free(image);

    return 0;
}
