constant sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE
   | CLK_ADDRESS_CLAMP | CLK_FILTER_NEAREST;

__kernel void simple_image(read_only image2d_t src_image,
                     write_only image2d_t dst_image) {

   float4 pixel;

   /* Determine output coordinate */
   int2 output_coord = (int2)
      (get_global_id(0),
       get_global_id(1));

   int2 input_coord = output_coord;

   pixel = read_imagef(src_image, sampler,
       input_coord);

   float avg = (pixel.x + pixel.y + pixel.z) / 3.0f;
   pixel.x = avg;
   pixel.y = avg;
   pixel.z = avg;

   write_imagef(dst_image, output_coord, pixel);
}
