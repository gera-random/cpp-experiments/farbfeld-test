#include "copyfun.h"

#include <arpa/inet.h>

uint32_t get_pixel_pos(uint32_t x, uint32_t y, uint32_t width) {
    uint32_t pixel_pos_dest = 4 * (y * width + x);
    return pixel_pos_dest;
}

void put_pixel(const uint16_t *rgba, uint16_t *dest, uint32_t pixel_pos_dest) {
    for (uint32_t k = 0; k < 4; k++) {
        dest[pixel_pos_dest + k] = htons(rgba[k]);
    }
}

void copy_without_transform(const uint16_t *rgba, uint32_t x, uint32_t y,
                            uint32_t width, uint32_t height,
                            uint16_t *dest) {
    uint32_t pixel_pos_dest = get_pixel_pos(x, y, width);

    put_pixel(rgba, dest, pixel_pos_dest);
}

void copy_flip_vertical(const uint16_t *rgba, uint32_t x, uint32_t y,
                        uint32_t width, uint32_t height,
                        uint16_t *dest) {
    uint32_t pixel_pos_dest = get_pixel_pos(x, height - y - 1, width);

    put_pixel(rgba, dest, pixel_pos_dest);
}


void copy_flip_horizontal(const uint16_t *rgba, uint32_t x, uint32_t y,
                          uint32_t width, uint32_t height,
                          uint16_t *dest) {
    uint32_t pixel_pos_dest = get_pixel_pos(width - 1 - x, y, width);

    put_pixel(rgba, dest, pixel_pos_dest);
}

void copy_flip_horizontal_and_vertical(const uint16_t *rgba, uint32_t x, uint32_t y,
                                       uint32_t width, uint32_t height,
                                       uint16_t *dest) {
    uint32_t pixel_pos_dest = get_pixel_pos(width - 1 - x, height - y - 1, width);

    put_pixel(rgba, dest, pixel_pos_dest);
}