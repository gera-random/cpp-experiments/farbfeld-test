#pragma once

#include <stdint.h>

void
copy_without_transform(const uint16_t *rgba, uint32_t dest_x, uint32_t dest_y,
                            uint32_t dest_width, uint32_t dest_height,
                            uint16_t *dest);

void
copy_flip_vertical(const uint16_t *rgba, uint32_t x, uint32_t y,
                   uint32_t width, uint32_t height,
                   uint16_t *dest);

void copy_flip_horizontal(const uint16_t *rgba, uint32_t x, uint32_t y,
                          uint32_t width, uint32_t height,
                          uint16_t *dest);

void copy_flip_horizontal_and_vertical(const uint16_t *rgba, uint32_t x, uint32_t y,
                                       uint32_t width, uint32_t height,
                                       uint16_t *dest);