#pragma once

#include <stdlib.h>
#include <stdint.h>

void
rgba_randomize(uint16_t rgba[4]);

void
rgba_invert(uint16_t rgba[4]);

void
rgba_avg_grey(uint16_t rgba[4]);

void
rgba_increase_contrast(uint16_t rgba[4]);

void
rgba_reduce_contrast(uint16_t rgba[4]);