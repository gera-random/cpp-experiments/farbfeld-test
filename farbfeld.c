// based on https://tools.suckless.org/farbfeld/examples/invert.c

#include "farbfeld.h"

uint16_t *farbfeld_read(char *input_file_name, uint32_t *hdr, uint32_t *width, uint32_t *height) {
    FILE *input_file = fopen(input_file_name, "r");

    /* read header */
    if (input_file == NULL || fread(hdr, sizeof(*hdr), HEADER_LENGTH, input_file) != HEADER_LENGTH) {
        goto readerr;
    }
    if (memcmp("farbfeld", hdr, sizeof("farbfeld") - 1) != 0) {
        fprintf(stderr, "invalid magic value\n");
        exit(1);
    }
    *(width) = ntohl(hdr[2]);
    *(height) = ntohl(hdr[3]);

    /* read data */
    const uint32_t pixels = *width * *height;
    uint16_t *image = malloc(sizeof(uint16_t) * 4 * *width * *height);
    if (fread(image, sizeof(uint16_t) * 4, pixels,
              input_file) != pixels) {
        goto readerr;
    }

    if (fclose(input_file)) {
        fprintf(stderr, "fclose: %s\n",
                strerror(errno));
        exit(1);
    }

    return image;
    readerr:
    errno = 1;
    return NULL;
}

void farbfeld_write(char *output_file_name, uint32_t *hdr, uint16_t *image, uint32_t width,
                    uint32_t height) {
    FILE *output_file = fopen(output_file_name, "w");

    /* write header */
    if (output_file == NULL || fwrite(hdr, sizeof(*hdr), HEADER_LENGTH, output_file) != HEADER_LENGTH) {
        goto writerr;
    }

    /* write data */
    const uint32_t pixels = width * height;
    if (fwrite(image, sizeof(uint16_t) * 4, pixels,
               output_file) != pixels) {
        goto writerr;
    }

    if (fclose(output_file)) {
        fprintf(stderr, "fclose: %s\n",
                strerror(errno));
        exit(1);
    }

    return;
    writerr:
    errno = 1;
}

void
farbfeld_process(void(*func)(uint16_t[4]), uint16_t *image, uint32_t width, uint32_t height,
                 const uint32_t *real_width) {
    if (real_width == NULL)
        real_width = &width;
    uint32_t i, j, k;
    uint16_t rgba[4];
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            uint32_t pixel_pos = 4 * (i * *real_width + j);

            for (k = 0; k < 4; k++) {
                rgba[k] = ntohs(image[pixel_pos + k]);
            }

            func(rgba);

            for (k = 0; k < 4; k++) {
                image[pixel_pos + k] = htons(rgba[k]);
            }
        }
    }
}

uint16_t *
farbfeld_copy(void(*copy_func)(const uint16_t *rgba, uint32_t dest_x, uint32_t dest_y,
                               uint32_t dest_width, uint32_t dest_height,
                               uint16_t *dest),
              const uint16_t *src, uint32_t width, uint32_t height,
              const uint32_t *real_width) {
    if (real_width == NULL)
        real_width = &width;

    uint16_t *dest = malloc(sizeof(uint16_t) * 4 * width * height);

    uint32_t i, j, k;
    uint16_t rgba[4];
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            uint32_t pixel_pos_src = 4 * (i * *real_width + j);

            for (k = 0; k < 4; k++) {
                rgba[k] = ntohs(src[pixel_pos_src + k]);
            }

            copy_func(rgba, j, i, width, height, dest);
        }
    }

    return dest;
}

uint32_t *farbfeld_make_header(uint32_t width, uint32_t height) {
    uint32_t *hdr = malloc(HEADER_LENGTH * sizeof(uint32_t));
    strcpy((char *) hdr, "farbfeld");
    hdr[2] = htonl(width);
    hdr[3] = htonl(height);
    return hdr;
}

uint16_t *
farbfeld_generate_image(uint16_t *(*get_pixel_rgba)(uint32_t x, uint32_t y, uint32_t width, uint32_t height),
                        uint32_t width,
                        uint32_t height) {
    uint16_t *image = malloc(sizeof(uint16_t) * 4 * width * height);
    uint32_t i, j, k;
    uint16_t *rgba;
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            uint32_t pixel_pos = 4 * (i * width + j);

            rgba = get_pixel_rgba(j, i, width, height);

            for (k = 0; k < 4; k++) {
                image[pixel_pos + k] = htons(rgba[k]);
            }

            free(rgba);
        }
    }

    return image;
}

uint16_t *
farbfeld_get_region(uint16_t *image, uint32_t original_width, uint32_t original_height,
                    float x1, float y1, float x2, float y2,
                    uint32_t *ret_width, uint32_t *ret_height) {
    uint32_t ix1 = x1 * original_width;
    uint32_t iy1 = y1 * original_height;
    uint32_t ix2 = x2 * original_width;
    uint32_t iy2 = y2 * original_height;
    if (ret_width != NULL) *ret_width = ix2 - ix1;
    if (ret_height != NULL) *ret_height = iy2 - iy1;
    return image + (ix1 * original_width + iy1) * 4;
}