#include "rgbafun.h"

#define MIN(x, y) (((x) < (y)) ? (x) : (y))

void
rgba_randomize(uint16_t rgba[4]) {
    rgba[0] = rand() % UINT16_MAX;
    rgba[1] = rand() % UINT16_MAX;
    rgba[2] = rand() % UINT16_MAX;
    rgba[3] = rand() % UINT16_MAX;
}

void
rgba_invert(uint16_t rgba[4]) {
    rgba[0] = UINT16_MAX - rgba[0];
    rgba[1] = UINT16_MAX - rgba[1];
    rgba[2] = UINT16_MAX - rgba[2];
}

void
rgba_avg_grey(uint16_t rgba[4]) {
    uint16_t avg = (rgba[0] + rgba[1] + rgba[2]) / 3;
    rgba[0] = avg;
    rgba[1] = avg;
    rgba[2] = avg;
}

void
rgba_increase_contrast(uint16_t rgba[4]) {
    const float k = 1.4f;
    rgba[0] = MIN(rgba[0] * k, UINT16_MAX);
    rgba[1] = MIN(rgba[1] * k, UINT16_MAX);
    rgba[2] = MIN(rgba[2] * k, UINT16_MAX);
}

void
rgba_reduce_contrast(uint16_t rgba[4]) {
    const float k = 1.4f;
    rgba[0] = (float)rgba[0] / k;
    rgba[1] = (float)rgba[1] / k;
    rgba[2] = (float)rgba[2] / k;
}