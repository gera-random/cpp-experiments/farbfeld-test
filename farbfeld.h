// based on https://tools.suckless.org/farbfeld/examples/invert.c

#pragma once

#include <arpa/inet.h>

#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define HEADER_LENGTH 4
#define LEN(x) (sizeof (x) / sizeof *(x))

uint16_t *farbfeld_read(char *input_file_name, uint32_t *hdr, uint32_t *width, uint32_t *height);

uint16_t *
farbfeld_generate_image(uint16_t *(*get_pixel_rgba)(uint32_t x, uint32_t y, uint32_t width, uint32_t height),
                        uint32_t width,
                        uint32_t height);

void
farbfeld_write(char *output_file_name, uint32_t *hdr, uint16_t *image, uint32_t width,
               uint32_t height);

void
farbfeld_process(void(*func)(uint16_t[4]), uint16_t *image, uint32_t width, uint32_t height,
                 const uint32_t *real_width);

uint16_t *
farbfeld_copy(void(*copy_func)(const uint16_t *rgba, uint32_t dest_x, uint32_t dest_y,
                               uint32_t dest_width, uint32_t dest_height,
                               uint16_t *dest),
              const uint16_t *src, uint32_t width, uint32_t height,
              const uint32_t *real_width);


uint32_t *farbfeld_make_header(uint32_t width, uint32_t height);

uint16_t *
farbfeld_get_region(uint16_t *image, uint32_t original_width, uint32_t original_height,
                    float x1, float y1, float x2, float y2,
                    uint32_t *ret_width, uint32_t *ret_height);