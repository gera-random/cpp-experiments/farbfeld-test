#include <time.h>
#include "farbfeld.h"
#include "rgbafun.h"

int
main(int argc, char *argv[]) {
    srand(clock());

    /* arguments */
    if (argc != 3) {
        fprintf(stderr, "usage: %s input_file output_file\n", argv[0]);
        return 1;
    }

    uint32_t width = 0, height = 0;
    uint32_t hdr[4];
    uint16_t *image = farbfeld_read(argv[1], hdr, &width, &height);
    if (image == NULL) {
        fprintf(stderr, "error reading image: %s\n", argv[1]);
    }

    uint32_t region_width, region_height;
    uint16_t *image_region = farbfeld_get_region(image, width, height, 0.2f, 0.2f, 0.8f, 0.8f, &region_width,
                                                 &region_height);
    farbfeld_process(rgba_invert, image_region, region_width, region_height, &width);
    farbfeld_process(rgba_reduce_contrast, image, width, height, NULL);
    farbfeld_process(rgba_avg_grey, image, width, height, NULL);

    farbfeld_write(argv[2], hdr, image, width, height);
    if (errno == 1) {
        fprintf(stderr, "error writing image: %s\n", argv[2]);
    }

    free(image);

    return 0;
}
